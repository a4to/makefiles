
# This repository contains various makefiles for different purposes

### Icon Makefile (icomf)

##### This makefile is used to generate various icons from a single SVG file.

##### TODO:
+ **Replace every instance of <app_name> with your application name** *sed -i 's/app_name/<your_app_name>/g' Makefile*
+ **Replace the SVGs in the Makefile with your own SVGs (just copy the text contained within a regular svg file)**

<p><img src="https://concise.cc/packages/previews/makefiles/icomf.png"></p>


