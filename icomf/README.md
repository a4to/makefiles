
## Icon Makefile (icomf)

This makefile is used to generate various icons from a single SVG file.

***

### TODO:
+ **Replace every instance of <app_name> with your application name** *sed -i 's/app_name/<your_app_name>/g' Makefile*
+ **Replace the SVGs in the Makefile with your own SVGs (just copy the text contained within a regular svg file)**

***

      [32m[+] [1;33mUsage: [0mmake [1;35m[[36mall[0m|[36mreact[0m|[36micon[0m|[36micons[0m|[36mpngs[0m|[36msplash[0m|[36madaptive[0m|[36mapple[0m|[36mwebapp[0m|[36mhelp[0m][0m

             [35mall[0m: icon16.png icon24.png icon32.png icon48.png icon64.png icon128.png icon192.png icon256.png icon512.png icon1024.png
                    logo192.png logo256.png logo512.png splash.png adaptive-icon.png apple-touch-icon.png
                    hicolor/1024x1024/apps/<app_name>.png
                    hicolor/128x128/apps/<app_name>.png
                    hicolor/16x16/apps/<app_name>.png
                    hicolor/192x192/apps/<app_name>.png
                    hicolor/22x22/apps/<app_name>.png
                    hicolor/24x24/apps/<app_name>.png
                    hicolor/256x256/apps/<app_name>.png
                    hicolor/32x32/apps/<app_name>.png
                    hicolor/36x36/apps/<app_name>.png
                    hicolor/48x48/apps/<app_name>.png
                    hicolor/512x512/apps/<app_name>.png
                    hicolor/64x64/apps/<app_name>.png
                    hicolor/72x72/apps/<app_name>.png
                    hicolor/96x96/apps/<app_name>.png

          [35mwepapp[0m: icon16.png icon24.png icon32.png icon48.png icon64.png icon128.png icon192.png icon256.png icon512.png icon1024.png
                    logo192.png logo256.png logo512.png splash.png adaptive-icon.png apple-touch-icon.png

           [35mlinux[0m: (all hicolor icons shown above)

           [35mreact[0m: logo256.png logo192.png favicon.png
            [35micon[0m: icon.svg adaptive-icon.svg splash.svg
            [35mpngs[0m: icon16.png icon24.png icon32.png icon48.png icon64.png icon128.png icon192.png icon256.png icon512.png icon1024.png splash.png adaptive-icon.png
          [35msplash[0m: splash.png
        [35madaptive[0m: adaptive-icon.png
           [35mapple[0m: apple-touch-icon.png

            [35mhelp[0m: Shows this help message

